<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OWNDAYS</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" integrity="sha512-oc9+XSs1H243/FRN9Rw62Fn8EtxjEYWHXRvjS43YtueEewbS6ObfXcJNyohjHqVKFPoXXUxwc+q1K7Dee6vv9g==" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <header class="header">
        <div class="container">
            <img src="{{ asset('img/owndays.svg') }}" alt="OWNDAYS" class="header__logo">
        </div>
    </header>
<main class="page page__not-found">
    <div class="container py-5">
        <h1>{{ __($sorry) }}</h1>
        <p class="my-5">
            {{  __($title) }}
            <span class="d-block"> {{ __($body) }}</span>
        </p>
        <a href="{{ $url }}" class="btn btn--default">{{  __($button) }}</a>
    </div>
</main>
</body>
</html>
