<?php 

return [
    'product-not-found' => "Sorry...!",
    'product-not-found-title' => 'Can\'t find product.',
    'product-not-found-body' => 'Please try to another product.',
    'product-not-found-button' => 'Try to find another product',
    'store-not-found' => "Sorry...!",
    'store-not-found-title' => 'Can\'t find store.',
    'store-not-found-body' => 'Please try scanning the QR code again.',
    'store-not-found-button' => 'Try to find another store.'
];