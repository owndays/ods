<?php 

return [
    'product-not-found' => "商品が見つかりません。",
    'product-not-found-title' => 'オンラインストアでの取扱いがありません。',
    'product-not-found-body' => '他の商品をお試しください。',
    'product-not-found-button' => '他の商品を探してみる',
    'store-not-found' => "店舗が見つかりません。",
    'store-not-found-title' => '指定の店舗が見つかりません。',
    'store-not-found-body' => '再度QRコードをスキャンしてください。',
    'store-not-found-button' => '他の店舗を探してみる'
];