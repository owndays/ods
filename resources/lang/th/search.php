<?php 

return [
    'product-not-found' => "ไม่พบสินค้า",
    'product-not-found-title' => 'ไม่พบสินค้า',
    'product-not-found-body' => 'กรุณาลองสินค้าอื่นๆ',
    'product-not-found-button' => 'ลองค้นหาสินค้าอื่นๆ',
    'store-not-found' => "ไม่พบร้านค้า",
    'store-not-found-title' => 'ไม่พบร้านค้า',
    'store-not-found-body' => 'โปรดสแกน QR code อีกครั้ง',
    'store-not-found-button' => 'โปรดค้นหาร้านค้าอื่น'
];