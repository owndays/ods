<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (env('APP_ENV') === 'production') {
        $redirectUrl = 'https://www.owndays.com';
    } else {
        $redirectUrl = 'https://preview.owndays.com';
    }

    return redirect()->to($redirectUrl);
});

Route::get('test', function () {
    return view('not-found');
});

//商品タグ用QR
Route::get('p/{code}' , 'ProductSearchController@handleRedirectByCode');
//http://127.0.0.1:8000/p/jp2300105181023

//店舗受付用QR
Route::get('l/{code}', 'ShopReceiptionController@handleRedirectByCode');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

