<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class ProductSearchController extends Controller
{
    const DEFAULT_LANGUAGE = [
        'jp' => 'ja',
        'sg' => 'en'
    ];

    public function handleRedirectByCode($code)
    {
        list ($countryCode, $janCode) = $this->decodeFromParameter($code);
        $client = new Client();
        $defaultLanguage = isset(self::DEFAULT_LANGUAGE[$countryCode]) ? self::DEFAULT_LANGUAGE[$countryCode] : 'en';
        $fallbackUrl = env('APP_ENV') === 'production' ? "https://www.owndays.com/$countryCode/$defaultLanguage/products" : "https://preview.owndays.com/$countryCode/$defaultLanguage/products";

        try {
            $response = $client->request('GET', env('PRODUCT_URL'), [
                'headers' => [
                    'Accept' => 'Application/json'
                ],
                'json' => [
                    'country_code' => $countryCode,
                    'jan_code' => $janCode,
                ]
            ]);
            $response = json_decode($response->getBody());
            app()->setLocale($response->language_code);
   
            if ($response->status === true) {
                return redirect()->to($response->url);
            } else {
                return view('not-found')->with([
                    'url' => $response->url,
                    'sorry' => 'search.product-not-found',
                    'title' => 'search.product-not-found-title',
                    'body' => 'search.product-not-found-body',
                    'button' => 'search.product-not-found-button',
                ]);
            }
        } catch (Exception $e) {
            Log::error($e);
            return view('not-found')->with([
                'url' => $fallbackUrl,
            ]);
        }

    }

    private function decodeFromParameter($code)
    {
        $prefix = null;
        $suffix = null;
        if (strlen($code) > 2) {
            $arrayOfString = str_split($code, 2);
            $prefix = array_shift($arrayOfString);
            $suffix = implode('', $arrayOfString);
        }
        return [$prefix, $suffix];
    }
}
