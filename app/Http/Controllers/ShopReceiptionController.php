<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Psy\Readline\Hoa\Protocol;

class ShopReceiptionController extends Controller
{
    private $contentUrl = 'reception';
    private $Protocol = "https://";

    const DEFAULT_LANGUAGE = [
        'jp' => 'ja',
        'sg' => 'en',
        'tw' => 'zh_tw',
        'th' => 'th',
        'hk' => 'zh_hk',
        'ph' => 'en',
        'my' => 'en',
        'au' => 'en',
        'vn' => 'vi',
        'kh' => 'km',
        'id' => 'in',
    ];
    //
    public function handleRedirectByCode($code)
    {
        $client = new Client();
        //URL分解
        list ($countryCode, $shopCode) = $this->decodeFromParameter($code);
        //デフォルト言語設定
        $defaultLanguage = isset(self::DEFAULT_LANGUAGE[$countryCode]) ? self::DEFAULT_LANGUAGE[$countryCode] : 'en';

        $contentPath = $countryCode . "/" . $defaultLanguage . "/" . $this->contentUrl;
        
        $domain = env('APP_ENV') === 'production' ? "www.owndays.com/" : "preview.owndays.com/";
        $fallbackUrl = $this->Protocol . $domain . $contentPath . "?store=" . (int)$shopCode;

        //URL存在確認/
        try {
            /*
            $basicAuth = [
                'User-Agent: My User Agent',
                'Authorization: Basic ' . base64_encode("renewal:owndays3070"),
            ];
            
            $options = [
                'http' => [
                    'header' => implode("\r\n", $basicAuth)
                ]
            ];
            */
            //
            $tmpUrl = "https://$domain" . "$countryCode/$defaultLanguage/shops/" . (int)$shopCode;
            // var_dump($tmpUrl);
            $response = true;
            // $response = @file_get_contents($tmpUrl, false, stream_context_create($options));
            //$response = file_get_contents($tmpUrl);
            //var_dump($response);
            /*
            $response = $client->request('GET', env('RECEPTION_URL'), [
                'headers' => [
                    'Accept' => 'Application/json'
                ],
                'json' => [
                    'country_code' => $countryCode,
                    'store_code' => (int)$shopCode,
                ]
            ]);
            */
            if ($response) {
                return redirect()->to($fallbackUrl);
            } else {
                switch($countryCode) {
                    case 'jp':
                    case 'th':
                        $locale = self::DEFAULT_LANGUAGE[$countryCode];
                        break;
                    default:
                        $locale = self::DEFAULT_LANGUAGE['sg'];
                        break;
                }
                app()->setLocale($locale);
                return view('not-found')->with([
                    'url' => $domain,
                    'sorry' => 'search.store-not-found',
                    'title' => 'search.store-not-found-title',
                    'body' => 'search.store-not-found-body',
                    'button' => 'search.store-not-found-button',
                ]);
            }
        } catch (Exception $e) {
            Log::error($e);
            return view('not-found')->with([
                'url' => $fallbackUrl,
            ]);
        }
    }


    private function decodeFromParameter($code)
    {
        $prefix = null;
        $suffix = null;
        if (strlen($code) > 2) {
            $arrayOfString = str_split($code, 2);
            $prefix = array_shift($arrayOfString);
            $suffix = implode('', $arrayOfString);
        }
        return [$prefix, $suffix];
    }
}
